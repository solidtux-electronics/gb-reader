#!/usr/bin/env python3

from serial import Serial
from tqdm import trange

s = Serial('/dev/ttyACM0', baudrate=115200)
print(s.name)
s.write([0xAA])
name_b = s.read(16)
name = name_b.split(b'\0', 1)[0].decode('utf-8')
if len(name) == 0:
    name = 'unknown'
banks = int.from_bytes(s.read(2), byteorder='big')
print('Reading %dkb for game %s' % (banks * 16, name))
with open('%s.gb' % name, 'wb') as f:
    for i in trange(0x4000 * banks, unit_scale=True, unit_divisor=1024, unit='b'):
        byte = s.read()
        f.write(byte)
    f.flush()
