use cortex_m::asm;
use tm4c123x_hal::{gpio::*, prelude::*, serial::*, sysctl::*, time::Bps, Peripherals};

fn sleep(n: usize) {
    for _ in 0..n {
        asm::nop();
    }
}

#[derive(Debug)]
pub struct CartridgeType {
    pub bank_controller: Option<BankController>,
    pub ram: bool,
    pub battery: bool,
    pub timer: bool,
    pub rumble: bool,
    pub sensor: bool,
}

#[derive(Debug)]
pub enum BankController {
    MBC1,
    MBC2,
    MMM01,
    MBC3,
    MBC5,
    MBC6,
    MBC7,
    HuC1,
    HuC3,
}

impl CartridgeType {
    pub fn from_value(val: u8) -> Option<CartridgeType> {
        use crate::BankController::*;
        match val {
            0x00 => Some(CartridgeType {
                bank_controller: None,
                ram: false,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x01 => Some(CartridgeType {
                bank_controller: Some(MBC1),
                ram: false,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x02 => Some(CartridgeType {
                bank_controller: Some(MBC1),
                ram: true,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x03 => Some(CartridgeType {
                bank_controller: Some(MBC1),
                ram: true,
                battery: true,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x04 => Some(CartridgeType {
                bank_controller: Some(MBC2),
                ram: false,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x06 => Some(CartridgeType {
                bank_controller: Some(MBC2),
                ram: false,
                battery: true,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x08 => Some(CartridgeType {
                bank_controller: None,
                ram: true,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x09 => Some(CartridgeType {
                bank_controller: None,
                ram: true,
                battery: true,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x0B => Some(CartridgeType {
                bank_controller: Some(MMM01),
                ram: false,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x0C => Some(CartridgeType {
                bank_controller: Some(MMM01),
                ram: true,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x0D => Some(CartridgeType {
                bank_controller: Some(MMM01),
                ram: true,
                battery: true,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x0F => Some(CartridgeType {
                bank_controller: Some(MBC3),
                ram: false,
                battery: true,
                timer: true,
                rumble: false,
                sensor: false,
            }),
            0x10 => Some(CartridgeType {
                bank_controller: Some(MBC3),
                ram: true,
                battery: true,
                timer: true,
                rumble: false,
                sensor: false,
            }),
            0x11 => Some(CartridgeType {
                bank_controller: Some(MBC3),
                ram: false,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x12 => Some(CartridgeType {
                bank_controller: Some(MBC3),
                ram: true,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x13 => Some(CartridgeType {
                bank_controller: Some(MBC3),
                ram: true,
                battery: true,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x19 => Some(CartridgeType {
                bank_controller: Some(MBC5),
                ram: false,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x1A => Some(CartridgeType {
                bank_controller: Some(MBC5),
                ram: true,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x1B => Some(CartridgeType {
                bank_controller: Some(MBC5),
                ram: true,
                battery: true,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x1C => Some(CartridgeType {
                bank_controller: Some(MBC5),
                ram: false,
                battery: false,
                timer: false,
                rumble: true,
                sensor: false,
            }),
            0x1D => Some(CartridgeType {
                bank_controller: Some(MBC5),
                ram: true,
                battery: false,
                timer: false,
                rumble: true,
                sensor: false,
            }),
            0x1E => Some(CartridgeType {
                bank_controller: Some(MBC5),
                ram: true,
                battery: true,
                timer: false,
                rumble: true,
                sensor: false,
            }),
            0x20 => Some(CartridgeType {
                bank_controller: Some(MBC6),
                ram: false,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0x22 => Some(CartridgeType {
                bank_controller: Some(MBC7),
                ram: true,
                battery: true,
                timer: false,
                rumble: true,
                sensor: true,
            }),
            0xFE => Some(CartridgeType {
                bank_controller: Some(HuC3),
                ram: false,
                battery: false,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            0xFF => Some(CartridgeType {
                bank_controller: Some(HuC1),
                ram: true,
                battery: true,
                timer: false,
                rumble: false,
                sensor: false,
            }),
            _ => None,
        }
    }
}

pub struct Cartridge {
    wr: gpiob::PB5<Output<PushPull>>,
    rd: gpiob::PB0<Output<PushPull>>,
    cs: gpiod::PD0<Output<PushPull>>,
    add: (
        gpiob::PB1<Output<PushPull>>,
        gpiof::PF2<Output<PushPull>>,
        gpioe::PE4<Output<PushPull>>,
        gpiod::PD2<Output<PushPull>>,
        gpioe::PE5<Output<PushPull>>,
        gpiod::PD3<Output<PushPull>>,
        gpiob::PB4<Output<PushPull>>,
        gpioe::PE1<Output<PushPull>>,
        gpioa::PA5<Output<PushPull>>,
        gpioe::PE2<Output<PushPull>>,
        gpioa::PA6<Output<PushPull>>,
        gpioe::PE3<Output<PushPull>>,
        gpioa::PA7<Output<PushPull>>,
        gpiof::PF1<Output<PushPull>>,
        gpiof::PF3<Output<PushPull>>,
        gpiob::PB2<Output<PushPull>>,
    ),
    data: (
        gpiob::PB3<InputOutput>,
        gpioc::PC4<InputOutput>,
        gpioc::PC5<InputOutput>,
        gpioc::PC6<InputOutput>,
        gpiob::PB7<InputOutput>,
        gpioc::PC7<InputOutput>,
        gpiob::PB6<InputOutput>,
        gpiod::PD6<InputOutput>,
    ),
    rst: gpioa::PA4<Output<PushPull>>,
}

macro_rules! set_bit {
    ($field:expr, $num:tt, $val:ident) => {
        if $val & (1 << $num) == 0 {
            $field.$num.set_low();
        } else {
            $field.$num.set_high();
        }
    };
}

macro_rules! get_bit {
    ($field:expr, $num:tt) => {
        if $field.$num.is_high() {
            1
        } else {
            0
        }
    };
}

impl Cartridge {
    pub fn new() -> (
        Cartridge,
        Serial<UART0, impl TxPin<UART0>, impl RxPin<UART0>, (), ()>,
    ) {
        let p = Peripherals::take().unwrap();
        let mut sc = p.SYSCTL.constrain();

        sc.clock_setup.oscillator = Oscillator::Main(
            CrystalFrequency::_16mhz,
            SystemClock::UsePll(PllOutputFrequency::_80_00mhz),
        );
        let clocks = sc.clock_setup.freeze();

        let mut porta = p.GPIO_PORTA.split(&sc.power_control);
        let portb = p.GPIO_PORTB.split(&sc.power_control);
        let portc = p.GPIO_PORTC.split(&sc.power_control);
        let portd = p.GPIO_PORTD.split(&sc.power_control);
        let porte = p.GPIO_PORTE.split(&sc.power_control);
        let portf = p.GPIO_PORTF.split(&sc.power_control);

        let mut wr = portb.pb5.into_push_pull_output();
        let mut rd = portb.pb0.into_push_pull_output();
        let mut cs = portd.pd0.into_push_pull_output();
        let add = (
            portb.pb1.into_push_pull_output(),
            portf.pf2.into_push_pull_output(),
            porte.pe4.into_push_pull_output(),
            portd.pd2.into_push_pull_output(),
            porte.pe5.into_push_pull_output(),
            portd.pd3.into_push_pull_output(),
            portb.pb4.into_push_pull_output(),
            porte.pe1.into_push_pull_output(),
            porta.pa5.into_push_pull_output(),
            porte.pe2.into_push_pull_output(),
            porta.pa6.into_push_pull_output(),
            porte.pe3.into_push_pull_output(),
            porta.pa7.into_push_pull_output(),
            portf.pf1.into_push_pull_output(),
            portf.pf3.into_push_pull_output(),
            portb.pb2.into_push_pull_output(),
        );
        let data = (
            portb.pb3.into_input_output(),
            portc.pc4.into_input_output(),
            portc.pc5.into_input_output(),
            portc.pc6.into_input_output(),
            portb.pb7.into_input_output(),
            portc.pc7.into_input_output(),
            portb.pb6.into_input_output(),
            portd.pd6.into_input_output(),
        );
        let mut rst = porta.pa4.into_push_pull_output();
        rst.set_high();
        wr.set_high();
        rd.set_high();
        let serial = Serial::uart0(
            p.UART0,
            porta.pa1.into_af_push_pull(&mut porta.control),
            porta.pa0.into_af_push_pull(&mut porta.control),
            (),
            (),
            115200_u32.bps(),
            NewlineMode::Binary,
            &clocks,
            &sc.power_control,
        );
        (
            Cartridge {
                wr,
                rd,
                cs,
                add,
                data,
                rst,
            },
            serial,
        )
    }

    pub fn get_type(&mut self) -> Option<CartridgeType> {
        CartridgeType::from_value(self.read_data(0x147))
    }

    pub fn get_banks(&mut self, bc: &Option<BankController>) -> Option<u16> {
        if bc.is_none() {
            return Some(2);
        }
        match self.read_data(0x148) {
            0x00 => Some(0),
            0x01 => Some(4),
            0x02 => Some(8),
            0x03 => Some(16),
            0x04 => Some(32),
            0x05 => {
                if let Some(BankController::MBC1) = bc {
                    Some(63)
                } else {
                    Some(64)
                }
            }
            0x06 => {
                if let Some(BankController::MBC1) = bc {
                    Some(125)
                } else {
                    Some(128)
                }
            }
            0x07 => Some(256),
            0x08 => Some(512),
            0x52 => Some(72),
            0x53 => Some(80),
            0x53 => Some(80),
            _ => None,
        }
    }

    pub fn set_add(&mut self, add: u16) {
        set_bit!(self.add, 0, add);
        set_bit!(self.add, 1, add);
        set_bit!(self.add, 2, add);
        set_bit!(self.add, 3, add);
        set_bit!(self.add, 4, add);
        set_bit!(self.add, 5, add);
        set_bit!(self.add, 6, add);
        set_bit!(self.add, 7, add);
        set_bit!(self.add, 8, add);
        set_bit!(self.add, 9, add);
        set_bit!(self.add, 10, add);
        set_bit!(self.add, 11, add);
        set_bit!(self.add, 12, add);
        set_bit!(self.add, 13, add);
        set_bit!(self.add, 14, add);
        set_bit!(self.add, 15, add);
    }

    pub fn write_data(&mut self, add: u16, data: u8) {
        self.rd.set_high();
        self.wr.set_low();
        self.set_add(add);
        self.data.0.to_output();
        self.data.1.to_output();
        self.data.2.to_output();
        self.data.3.to_output();
        self.data.4.to_output();
        self.data.5.to_output();
        self.data.6.to_output();
        self.data.7.to_output();
        sleep(1_000);
        set_bit!(self.data, 0, data);
        set_bit!(self.data, 1, data);
        set_bit!(self.data, 2, data);
        set_bit!(self.data, 3, data);
        set_bit!(self.data, 4, data);
        set_bit!(self.data, 5, data);
        set_bit!(self.data, 6, data);
        set_bit!(self.data, 7, data);
        sleep(100_000);
        self.rd.set_high();
        self.wr.set_high();
        sleep(1_000);
        self.data.0.to_input();
        self.data.1.to_input();
        self.data.2.to_input();
        self.data.3.to_input();
        self.data.4.to_input();
        self.data.5.to_input();
        self.data.6.to_input();
        self.data.7.to_input();
    }

    pub fn read_data(&mut self, add: u16) -> u8 {
        self.rd.set_low();
        self.wr.set_high();
        self.set_add(add);
        sleep(1_000);
        let mut res = 0;
        self.data.0.to_input();
        self.data.1.to_input();
        self.data.2.to_input();
        self.data.3.to_input();
        self.data.4.to_input();
        self.data.5.to_input();
        self.data.6.to_input();
        self.data.7.to_input();
        res |= get_bit!(self.data, 0) << 0;
        res |= get_bit!(self.data, 1) << 1;
        res |= get_bit!(self.data, 2) << 2;
        res |= get_bit!(self.data, 3) << 3;
        res |= get_bit!(self.data, 4) << 4;
        res |= get_bit!(self.data, 5) << 5;
        res |= get_bit!(self.data, 6) << 6;
        res |= get_bit!(self.data, 7) << 7;
        self.rd.set_high();
        self.wr.set_high();
        res
    }
}
