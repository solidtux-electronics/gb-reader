#![no_std]
#![no_main]

extern crate panic_semihosting;

use core::fmt::Write;
use cortex_m::asm;
use cortex_m_rt::entry;
use cortex_m_semihosting::{hprint, hprintln};
use tm4c123x_hal::{gpio::*, prelude::*, serial::*, sysctl::*, time::Bps, Peripherals};

mod gameboy;

use crate::gameboy::*;

#[entry]
fn main() -> ! {
    let (mut cart, mut serial) = Cartridge::new();
    loop {
        if let Ok(0xAA) = serial.read() {
            for a in 0x134..=0x143 {
                let d = cart.read_data(a);
                serial.write_all(&mut [d]);
            }
            match cart.get_type() {
                Some(t) => {
                    if let Some(banks) = cart.get_banks(&t.bank_controller) {
                        hprintln!("{:?}: {}", t.bank_controller, banks);
                        serial.write_all(&mut [(banks >> 8) as u8, (banks & 0xFF) as u8]);
                        match t.bank_controller {
                            None => {
                                for a in 0..=0x7FFF {
                                    let d = cart.read_data(a);
                                    serial.write_all(&mut [d]);
                                }
                            }
                            Some(BankController::MBC1) => {
                                cart.write_data(0x2100, 0);
                                for a in 0..=0x3FFF {
                                    let d = cart.read_data(a);
                                    serial.write_all(&mut [d]);
                                }
                                let mut b = 1;
                                let mut banks = banks;
                                while b < banks {
                                    if [0x20, 0x40, 0x60].contains(&b) {
                                        banks += 1;
                                    } else {
                                        cart.write_data(0x2100, (b & 0xFF) as u8);
                                        //cart.write_data(0x3000, (b >> 8) as u8);
                                        for a in 0x4000..=0x7FFF {
                                            let d = cart.read_data(a);
                                            serial.write_all(&mut [d]);
                                        }
                                    }
                                    b += 1;
                                }
                            }
                            Some(BankController::MBC5) => {
                                cart.write_data(0x2100, 0);
                                for a in 0..=0x3FFF {
                                    let d = cart.read_data(a);
                                    serial.write_all(&mut [d]);
                                }
                                for b in 1..banks {
                                    cart.write_data(0x2100, (b & 0xFF) as u8);
                                    //cart.write_data(0x3000, (b >> 8) as u8);
                                    for a in 0x4000..=0x7FFF {
                                        let d = cart.read_data(a);
                                        serial.write_all(&mut [d]);
                                    }
                                }
                            }
                            _ => serial.write_all(&mut [0, 0]),
                        }
                    }
                }
                _ => serial.write_all(&mut [0x00, 0x00]),
            }
            cart.read_data(0);
        }
    }
}
